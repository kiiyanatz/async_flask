from creepy import Crawler
import re
from pymongo import MongoClient

client = MongoClient()

# Create db
db = client.crawler_db
collection = db.crawl_collection


class MyCrawler(Crawler):

    def process_document(self, doc):
        if doc.status == 200:
            print '[%d] %s' % (doc.status, doc.url)

            count = re.findall("cord", doc.text, re.IGNORECASE)
            search_count = len(count)
            print search_count
            result = db.crawl_collection.insert({
                "url": doc.url,
                "keyword": "isis",
                "count": search_count,
                "content": doc.text
                }
            )
        else:
            pass
