"""
Demo Flask application to test the operation of Flask with socket.io

Aim is to create a webpage that is constantly updated with random numbers from a background python process.

30th May 2014
"""

# Start with a basic flask app webpage.
from flask_socketio import SocketIO, emit
from flask import Flask, render_template, url_for, \
copy_current_request_context, request, redirect
from random import random
from time import sleep
from threading import Thread, Event
import tweepy
import json


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = True


CONSUMER_KEY = 'FQbtDGTtl0BMuplpJsNb0WLLc'
CONSUMER_SECRET = 'FCwb55XKHzr53KxUwQqOiyVM65HaRiIMl14flz4SXUMsZg6n7l'
OAUTH_TOKEN = '3665986227-3XUwtD2uRX1xuwQ3bQZzN6PvofnfYdulEb3LoiO'
OAUTH_SECRET = 'qM3I8Q1H4aShMvwDYVPibpKfaFBDpE6a7Ti9lGZtjsxOg'


# Turn the flask app into a socketio app
socketio = SocketIO(app)


class TwitterStream(tweepy.StreamListener):
    def on_data(self, data):
        json_data = json.loads(data)
        socketio.emit('tweets', {'tweet': json_data}, namespace='/test')
        with open('tweets.json', 'w') as outfile:
            json.dump(data, outfile)
        print json_data['text']
        return True

    def on_error(self, status):
        print status


def stream(search_query):
    listener = TwitterStream()
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(OAUTH_TOKEN, OAUTH_SECRET)
    live_stream = tweepy.Stream(auth, listener)
    live_stream.filter(track=[search_query])

@socketio.on('connect', namespace='/test')
def connect():
    print('Client connected')


@socketio.on('disconnect', namespace='/test')
def disconnect():
    print('Client disconnected')


# Get search query and pass to all modules
@app.route('/search')
def search():
    search_query = request.args.get('search_query')
    stream(search_query)
    print search_query

@app.route('/')
def index():
    # only by sending this page first will
    # the client be connected to the socketio instance)
    return render_template('index.html')
