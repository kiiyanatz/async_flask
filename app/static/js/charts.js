// $(function () {
//     $('#kiiya').highcharts({
//         chart: {
//             type: 'bar'
//         },
//         title: {
//             text: 'Tweet distribution'
//         },
//         xAxis: {
//             categories: ['Apples', 'Bananas', 'Oranges']
//         },
//         yAxis: {
//             title: {
//                 text: 'Fruit eaten'
//             }
//         },
//         series: [{
//             name: 'Jane',
//             data: [1, 0, 4]
//         }, {
//             name: 'John',
//             data: [5, 7, 3]
//         }]
//     });
// });
// $(function () {
//     $('#kiiya').highcharts({
//         title: {
//             text: 'Monthly Average Temperature',
//             x: -20 //center
//         },
//         subtitle: {
//             text: 'Source: WorldClimate.com',
//             x: -20
//         },
//         xAxis: {
//             categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
//                 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
//         },
//         yAxis: {
//             title: {
//                 text: 'Temperature (°C)'
//             },
//             plotLines: [{
//                 value: 0,
//                 width: 1,
//                 color: '#808080'
//             }]
//         },
//         tooltip: {
//             valueSuffix: '°C'
//         },
//         legend: {
//             layout: 'vertical',
//             align: 'right',
//             verticalAlign: 'middle',
//             borderWidth: 0
//         },
//         series: [{
//             name: 'Tokyo',
//             data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
//         }, {
//             name: 'New York',
//             data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
//         }, {
//             name: 'Berlin',
//             data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
//         }, {
//             name: 'London',
//             data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
//         }]
//     });
// });
// $(function () {
//
//     // Give the points a 3D feel by adding a radial gradient
//     Highcharts.getOptions().colors = $.map(Highcharts.getOptions().colors, function (color) {
//         return {
//             radialGradient: {
//                 cx: 0.4,
//                 cy: 0.3,
//                 r: 0.5
//             },
//             stops: [
//                 [0, color],
//                 [1, Highcharts.Color(color).brighten(-0.2).get('rgb')]
//             ]
//         };
//     });
//
//     // Set up the chart
//     var chart = new Highcharts.Chart({
//         chart: {
//             renderTo: 'kiiya',
//             margin: 100,
//             type: 'scatter',
//             options3d: {
//                 enabled: true,
//                 alpha: 10,
//                 beta: 30,
//                 depth: 250,
//                 viewDistance: 5,
//                 fitToPlot: false,
//                 frame: {
//                     bottom: { size: 1, color: 'rgba(0,0,0,0.02)' },
//                     back: { size: 1, color: 'rgba(0,0,0,0.04)' },
//                     side: { size: 1, color: 'rgba(0,0,0,0.06)' }
//                 }
//             }
//         },
//         title: {
//             text: 'Draggable box'
//         },
//         subtitle: {
//             text: 'Click and drag the plot area to rotate in space'
//         },
//         plotOptions: {
//             scatter: {
//                 width: 10,
//                 height: 10,
//                 depth: 10
//             }
//         },
//         yAxis: {
//             min: 0,
//             max: 10,
//             title: null
//         },
//         xAxis: {
//             min: 0,
//             max: 10,
//             gridLineWidth: 1
//         },
//         zAxis: {
//             min: 0,
//             max: 10,
//             showFirstLabel: false
//         },
//         legend: {
//             enabled: false
//         },
//         series: [{
//             name: 'Reading',
//             colorByPoint: true,
//             data: [[1, 6, 5], [8, 7, 9], [1, 3, 4], [4, 6, 8], [5, 7, 7], [6, 9, 6], [7, 0, 5], [2, 3, 3], [3, 9, 8], [3, 6, 5], [4, 9, 4], [2, 3, 3], [6, 9, 9], [0, 7, 0], [7, 7, 9], [7, 2, 9], [0, 6, 2], [4, 6, 7], [3, 7, 7], [0, 1, 7], [2, 8, 6], [2, 3, 7], [6, 4, 8], [3, 5, 9], [7, 9, 5], [3, 1, 7], [4, 4, 2], [3, 6, 2], [3, 1, 6], [6, 8, 5], [6, 6, 7], [4, 1, 1], [7, 2, 7], [7, 7, 0], [8, 8, 9], [9, 4, 1], [8, 3, 4], [9, 8, 9], [3, 5, 3], [0, 2, 4], [6, 0, 2], [2, 1, 3], [5, 8, 9], [2, 1, 1], [9, 7, 6], [3, 0, 2], [9, 9, 0], [3, 4, 8], [2, 6, 1], [8, 9, 2], [7, 6, 5], [6, 3, 1], [9, 3, 1], [8, 9, 3], [9, 1, 0], [3, 8, 7], [8, 0, 0], [4, 9, 7], [8, 6, 2], [4, 3, 0], [2, 3, 5], [9, 1, 4], [1, 1, 4], [6, 0, 2], [6, 1, 6], [3, 8, 8], [8, 8, 7], [5, 5, 0], [3, 9, 6], [5, 4, 3], [6, 8, 3], [0, 1, 5], [6, 7, 3], [8, 3, 2], [3, 8, 3], [2, 1, 6], [4, 6, 7], [8, 9, 9], [5, 4, 2], [6, 1, 3], [6, 9, 5], [4, 8, 2], [9, 7, 4], [5, 4, 2], [9, 6, 1], [2, 7, 3], [4, 5, 4], [6, 8, 1], [3, 4, 0], [2, 2, 6], [5, 1, 2], [9, 9, 7], [6, 9, 9], [8, 4, 3], [4, 1, 7], [6, 2, 5], [0, 4, 9], [3, 5, 9], [6, 9, 1], [1, 9, 2]]
//         }]
//     });
//
//
//     // Add mouse events for rotation
//     $(chart.container).bind('mousedown.hc touchstart.hc', function (eStart) {
//         eStart = chart.pointer.normalize(eStart);
//
//         var posX = eStart.pageX,
//             posY = eStart.pageY,
//             alpha = chart.options.chart.options3d.alpha,
//             beta = chart.options.chart.options3d.beta,
//             newAlpha,
//             newBeta,
//             sensitivity = 5; // lower is more sensitive
//
//         $(document).bind({
//             'mousemove.hc touchdrag.hc': function (e) {
//                 // Run beta
//                 newBeta = beta + (posX - e.pageX) / sensitivity;
//                 chart.options.chart.options3d.beta = newBeta;
//
//                 // Run alpha
//                 newAlpha = alpha + (e.pageY - posY) / sensitivity;
//                 chart.options.chart.options3d.alpha = newAlpha;
//
//                 chart.redraw(false);
//             },
//             'mouseup touchend': function () {
//                 $(document).unbind('.hc');
//             }
//         });
//     });
//
// });
// $(function () {
//     $('#kiiya').highcharts({
//         chart: {
//             type: 'areaspline'
//         },
//         title: {
//             text: 'Keyword Mentions'
//         },
//         legend: {
//             layout: 'vertical',
//             align: 'left',
//             verticalAlign: 'top',
//             x: 150,
//             y: 100,
//             floating: true,
//             borderWidth: 1,
//             backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
//         },
//         xAxis: {
//             categories: [
//                 'Monday',
//                 'Tuesday',
//                 'Wednesday',
//                 'Thursday',
//                 'Friday',
//                 'Saturday',
//                 'Sunday'
//             ],
//             plotBands: [{ // visualize the weekend
//                 from: 4.5,
//                 to: 6.5,
//                 color: 'rgba(68, 170, 213, .2)'
//             }]
//         },
//         yAxis: {
//             title: {
//                 text: 'Number of Mentions'
//             }
//         },
//         tooltip: {
//             shared: true,
//             valueSuffix: ' units'
//         },
//         credits: {
//             enabled: false
//         },
//         plotOptions: {
//             areaspline: {
//                 fillOpacity: 0.5
//             }
//         },
//         series: [{
//             name: 'Positive',
//             data: [3, 4, 3, 5, 4, 10, 12]
//         }, {
//             name: 'Negative',
//             data: [1, 3, 4, 3, 3, 5, 4]
//         }]
//     });
// });
