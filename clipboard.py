"""
Demo Flask application to test the operation of Flask with socket.io

Aim is to create a webpage that is constantly updated with random numbers from a background python process.

30th May 2014
"""

# Start with a basic flask app webpage.
from flask.ext.socketio import SocketIO, emit
from flask import Flask, render_template, url_for, copy_current_request_context
from random import random
from time import sleep
from threading import Thread, Event
import tweepy
import json
from celery import Celery


app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = True

celery = Celery(app.import_name)
celery.conf.update(app.config)


CONSUMER_KEY = 'FQbtDGTtl0BMuplpJsNb0WLLc'
CONSUMER_SECRET = 'FCwb55XKHzr53KxUwQqOiyVM65HaRiIMl14flz4SXUMsZg6n7l'
OAUTH_TOKEN = '3665986227-3XUwtD2uRX1xuwQ3bQZzN6PvofnfYdulEb3LoiO'
OAUTH_SECRET = 'qM3I8Q1H4aShMvwDYVPibpKfaFBDpE6a7Ti9lGZtjsxOg'


#turn the flask app into a socketio app
socketio = SocketIO(app)

# class RandomThread(Thread):
#     def __init__(self):
#         self.delay = 1
#         super(RandomThread, self).__init__()

#     def randomNumberGenerator(self):
#         """
#         Generate a random number every 1 second and
#         emit to a socketio instance (broadcast)
#         Ideally to be run in a separate thread?
#         """
#         # infinite loop of magical random numbers
#         print "Making random numbers"
#         while not thread_stop_event.isSet():
#             number = round(random() * 10, 3)
#             print number
#             socketio.emit('newnumber', {'number': number}, namespace='/test')
#             sleep(self.delay)

#     def run(self):
#         self.randomNumberGenerator()


class TwitterStream(tweepy.StreamListener):
    def on_data(self, data):
        json_data = json.loads(data)
        socketio.emit('tweets', {'tweet': json_data}, namespace='/test')
        print json_data
        return True

    def on_error(self, status):
        print status


@celery.task()
def stream():
        listener = TwitterStream()
        auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
        auth.set_access_token(OAUTH_TOKEN, OAUTH_SECRET)
        live_stream = tweepy.Stream(auth, listener)
        live_stream.filter(track=['#ThanksgivingThursday'])


@app.route('/')
def index():
    # only by sending this page first will
    # the client be connected to the socketio instance
    return render_template('index.html')


@socketio.on('connect', namespace='/test')
def test_connect():
    print('Client connected')
    stream()


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')


if __name__ == '__main__':
    socketio.run(app)



$(document).ready(function(){
    //connect to the socket server.
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/test');
    var tweets_received = [];

    //receive details from server
    socket.on('tweets', function(msg) {
        console.log("Received tweet" + msg.tweet.text);
        //maintain a list of ten tweets
        if (tweets_received.length >= 10){
            tweets_received.shift()
        }
        tweets_received.push(msg.tweet.text);
        tweets_string = '';
        for (var i = 0; i < tweets_received.length; i++){
            tweets_string = tweets_string + '<div class="col-md-12">' + '<p>' + tweets_received[i].toString() + '</p>' + '</div>';
        }
        $('#tweet_text').html(tweets_string);
    });

});



$(document).ready(function(){
    //connect to the socket server.
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/test');
    var tweets_received = [];
    var profile_received = [];

    //receive details from server
    socket.on('tweets', function(msg) {
        console.log("Received tweet" + msg.tweet.text);
        console.log("Profile image " + msg.tweet.user['profile_image_url']);
        //maintain a list of ten tweets
        if (tweets_received.length >= 10 && profile_received.length >=0){
            tweets_received.shift()
            profile_received.shift()
        }
        tweets_received.push(msg.tweet.text);
        profile_received.push(msg.tweet.user['profile_image_url']);
        tweets_string = '';
        profile_string = '';
        // img = '<img src=" '+ msg.tweet.user['profile_image_url'] +'" class="img-circle img-responsive">';
        for (var i = 0; i < tweets_received.length; i++){
            tweets_string = tweets_string +
            '<div class="row"><div class="col-md-2"> <img src=" '+ profile_received[i].toString() +'" class="img-circle img-responsive"> </div><div class="col-md-10"> <p>' + tweets_received[i].toString() + '</p></div></div>';
            profile_string = profile_string + '<img src=" '+ profile_received[i].toString() +'" class="img-circle img-responsive">';
        }
        $('#tweet_text').html(tweets_string);
        $('#tweet_img').html()
    });

});


<!DOCTYPE html>
<html>
<head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/socket.io/0.9.16/socket.io.min.js"></script>
    <script src="static/js/application.js"></script>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <style>
        #imgs{
            background: #aaa;
        }
    </style>
</head>
<body>
<div class="jumbotron">
    <h1>Tweet Stream</h1>
</div>

<div class="container" id="tweet_text">

</div>
</body>
</html>

